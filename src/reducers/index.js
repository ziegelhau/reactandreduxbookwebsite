import { combineReducers } from 'redux';
import BooksReducer from './reducer_books';
import ActiveBook from "./reducer_active_book";

// State is always equal to an object with a key of books, and a value of an array of books
// any key we provide to the reducer, ends up as a key on our global state
const rootReducer = combineReducers({
  books: BooksReducer, // this reducer is responsible for creating this piece of state
  activeBook: ActiveBook 
});

export default rootReducer;
