// all reducers recieve these 2 arguments
// only called when an action occurs
// State argument is not application state, only the state
// this reducer is repsonsible for

// Set default case to null for first time code runs
export default function(state = null, action){

    switch(action.type){    
    case 'BOOK_SELECTED':
        return action.payload
    }

    return state
}