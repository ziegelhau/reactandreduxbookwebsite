// Action creater, needs to send to the reducers so they can potentially re-render

export function selectBook(book){
    // selectBook is an ActionCreater, it needs to return an action,
    // an object with a type property
    // Actions usually have 2 values, a type, and a payload
    // Type convention is usually an uppercase
    return {
        type: 'BOOK_SELECTED',
        payload: book
    };
}