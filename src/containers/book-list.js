import React, { Component } from 'react';
import {connect} from 'react-redux'; // is the glue that links react and redux
import { selectBook } from "../actions/index";
import {bindActionCreators} from "redux"; // makes sure the action flows the the reducers in our application

// Want to export the container and not bookList

class BookList extends Component {
renderList(){

    return this.props.books.map((book) => {
        return (
            <li 
            key={book.title} 
            onClick={() => this.props.selectBook(book)}
            className="list-group-item">
            {book.title}</li>
        )
    })
}

    render() {
        return (
            <ul className="list-group col-sm-4">
                {this.renderList()}
            </ul>
        )
    }
}

// returns an object which will be available in this.props (this.props.books)
// If the state ever changes, this cointaner will instantly rerender with a new list of books
function mapStateToProps(state){
    // Whatever is returned will show up as props inside of BookList
    return {
        books: state.books

    }
}

// Anything returned from this function will end up as props
// on the BookList container
function mapDispatchToProps(dispatch){
    // Whenever selectBook is called, the result should be passed to all of our
    // reducers.     
    // Passing in an object with a key: selectBook, and a value: selectBook
    return bindActionCreators({selectBook: selectBook}, dispatch)

}

// connect takes a function and a component and produces a container
// a container is aware of the state that is contained by redux
// Promot BookList from a component to a cointaner - it needs to know
// about this new dispatch method, selectBook. Make it available
// as a prop.
export default connect(mapStateToProps, mapDispatchToProps)(BookList); 